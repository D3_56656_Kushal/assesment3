const express= require('express')
const utils= require('../utils')

const route= express.Router()

route.get('/',(req,res)=>
{
    const conn= utils.getConnect()

    conn.query('select * from product',(err,data)=>
    {
        res.send(utils.getRes(err,data))
    })
})


route.post('/',(req,res)=>
{
    const conn= utils.getConnect()
    const{name,price}=req.body
    conn.query(`insert into product(name,price)values('${name}',${price})`,(err,data)=>
    {
        res.send(utils.getRes(err,data))
    })
})

route.put('/:id',(req,res)=>
{
    const conn= utils.getConnect()
    const{id}=req.params
    const{name,price}=req.body
    conn.query(`update product set name='${name}',price=${price} where id=${id}`,(err,data)=>
    {
        res.send(utils.getRes(err,data))
    })
})

route.delete('/:id',(req,res)=>
{
    const conn= utils.getConnect()
    const{id}=req.params
  
    conn.query(`delete from product where id=${id}`,(err,data)=>
    {
        res.send(utils.getRes(err,data))
    })
})
module.exports=route